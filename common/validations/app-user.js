module.exports = function (Appuser, server) {
    'use strict';

    const {
        isLength,
        isEmail,
        normalizeEmail
    } = require('validator');

    const moment = require('moment');

    const _ = require('lodash');
    const titleCase = require('title-case');

    const {
        validate,
    } = require("../helper/usefullMethods");




    const SECRET_PASSWORD = "SNAPHY_SYNAPSICA_SECRET_PASSWORD";      // Default password


    //----------------------------Loopback Validations..------------------------------------------

    // Email
    Appuser.validatesUniquenessOf('email', { message: "A user is already present with this email." });
    // Username - Unique, Ranged-Length-Constrained
    Appuser.validatesUniquenessOf("username", { message: "A user is already present with this username!" });
    Appuser.validatesLengthOf('username', {
        min: 3, max: 50, message: {
            min: "must contain more than 3 characters!",
            max: "must not contain more than 50 characters"
        }
    });
    // FirstName - Ranged-Length-Constrained
    Appuser.validatesLengthOf('firstName', {
        min: 3, max: 200, message: {
            min: "must contain more than 3 characters!",
            max: "must not contain more than 50 characters"
        }
    });
    // LastName - Ranged-Length-Constrained
    Appuser.validatesLengthOf('lastName', {
        min: 3, max: 200, message: {
            min: "must contain more than 3 characters!",
            max: "must not contain more than 50 characters"
        }
    });
    // OriUserId - Unique
    Appuser.validatesUniquenessOf("oriUserId", { message: "A user is already registered with this oriUserId" });

    // CricUserId - Unique
    Appuser.validatesUniquenessOf('cricUserId', { message: "A user is already registered with this cricUserId" });

    // Delete general validations for Email
    delete Appuser.validations.email;
    delete Appuser.validations.firstName;
    delete Appuser.validations.lastName;

    //----------------------------END Loopback Validation -----------------------------------------

    /**
     * Operation Hook -  before save
     */
    Appuser.observe('before save', function (ctx, next) {
        const instance = ctx.instance || ctx.data;                     // getting instance
        const currentInstance = ctx.currentInstance;                          // getting currentInstance
        const isNew = ctx.isNewInstance;


        /**
        * Populating added, updated, unique_number fields
        */
        if (ctx.isNewInstance) {                                                  // if new instance
            instance.added = moment().toDate();                            // set added = current time
            instance.updated = moment().toDate();                                   // set updated = current time
            instance.uniqueNumber = _.random(999999999, 9999999999).toString();    // set uniqueNumber = random generated unique number 
        } else {                                                                  // if is already present
            instance.updated = moment().toDate();                                   // set updated = current time
        }

        // set password to default
        instance.password = SECRET_PASSWORD;


        /**
        * Validation for "firstName"
        * title-case, length-constrained
        */
        if (instance.firstName) {
            const firstName = instance.firstName.toString();
            instance.firstName = firstName.trim();
        }

        /**
        * Validation for "lastName" - not required
        * title-cased, length-constrained
        */
        if (instance.lastName) {
            const lastName = instance.lastName.toString();
            instance.lastName = lastName.trim();
        }

        /**
        * Validation for "email"
        * Required, Unique, email-format-constrained
        */
        if (validate(instance, currentInstance, "email", isNew)) {                       // if Invalid
            if (instance.email) {                                                             // if email is present
                const email = instance.email.toString().toLowerCase();
                instance.email = email.trim();                                              // convert to lowercase, trim value
                instance.email = normalizeEmail(instance.email);                                // Sanitize email
                const check = isEmail(instance.email);                                          // Check for valid format
                if (!check) {                                                                     // if invalid
                    return next(new Error("Enter valid email address!"));                              // return error to next middleware
                }
            }
        }

        /**
        * Validation for "username"
        * Required, Unique, length-constrained
        */
        if (instance.username) {                                                                // if value is present in instance
            const username = instance.username.toString();
            instance.username = username.trim();                                                    // trim value         
        }

        /**
        * Validation for oriUserId
        */
        if (instance.oriUserId) {                                                        // if oriUserId present in instance
            const oriUserId = instance.oriUserId.toString();
            instance.oriUserId = oriUserId.trim();                                          // trim value
        }

        if (!validate(instance, currentInstance, "cricUserId", isNew)) {
            if (instance.oriUserId !== 'admin') {
                return next(new Error("cricUserId is required!"));
            }
        }
        next();
    });  // END - Operation Hook - before save
};