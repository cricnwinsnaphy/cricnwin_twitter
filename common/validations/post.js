module.exports = function (Post, server, helper) {
    'use strict';

    // require necessary modules
    const {
        isLength
    } = require('validator');

    const {
        validate
    } = require('../helper/usefullMethods');

    const moment = require('moment');
    const _ = require('lodash');
    const slug = require('slug');
    const Promise = require('bluebird');


    const STATUS = ["active", "inactive"];
    const STATUS_OBJ = {
        active: "active",
        inactive: "inactive",
    };

    const BASIC_SCORE = helper.getMainSettings("BASIC_SCORE");
    const ROWS = helper.getMainSettings("ROWS");
    const COLS = helper.getMainSettings("COLS");

    const AppUser = server.models.AppUser;


    //----------------------------Loopback Validations..------------------------------------------

    // Status - Inclusion-Constrained
    Post.validatesInclusionOf('status', {
        in: ['active', 'inactive'], message: 'is not allowed!'
    });

    //----------------------------END Loopback Validation -----------------------------------------


    /**
     * Operation Hook - before save
     */
    Post.observe('before save', function (ctx, next) {

        const instance = ctx.instance || ctx.data;              // get instance
        const currentInstance = ctx.currentInstance;            // get currentInstance
        const isNew = ctx.isNewInstance;


        /**
         * Populating added, updated
         */
        if (isNew) {                                                  // if new instance
            instance.added = moment().toDate();                             // set added    = current time
            instance.updated = moment().toDate();                             // set updated  = current time
            instance.uniqueNumber = _.random(999999999, 9999999999).toString();     // set uniqueNumber = random generated number
        } else {                                                                  // if not a new instance
            instance.updated = moment().toDate();                             // set updated  = current time
        }

        /**
         * Validations for "title" - not required
         * length-constrained
         */
        if (validate(instance, currentInstance, "title", isNew)) {                 // if valid
            if (instance.title) {                                                   // if title is present in instance
                const title = instance.title.toString();
                instance.title = title.trim();                                      // trim value 

                // Generating slug for SEO purpose....
                instance.slug = slug(instance.title).toLowerCase();
            }
        }


        /**
         * Validation for status
         * if present - must contain valid value
         **/
        if (validate(instance, currentInstance, "status", isNew)) {                // if Valid
            if (instance.status) {                                                    // if status is present in instance
                const status = instance.status.toString();
                instance.status = status.trim();                                    // trim value
                instance.status = instance.status.toLowerCase();                        // convert to lowercase      
            }
        }

        if (isNew) {
            if (!instance.countRecords) {
                const countRecords = new Array(ROWS);
                for (let i = 0; i < ROWS; i++) {
                    countRecords[i] = [0, 0, 0, 0];
                }
                instance.countRecords = countRecords;
            }
            if (!instance.netScoreRecords) {
                const netScoreRecords = new Array(ROWS);
                for (let i = 0; i < ROWS; i++) {
                    netScoreRecords[i] = [0, 0, 0, 0];
                }
                instance.netScoreRecords = netScoreRecords;
            }
            if (!instance.engagement) {
                instance.engagement = 0;
            }
            if (!instance.priority) {
                instance.priority = 0;
            }
        }

        if (instance.netScoreRecords) {
            instance.engagement = 0;
            for (let i = 0; i < ROWS; i++) {
                for (let j = 0; j < COLS; j++) {
                    instance.netScoreRecords[i][j] = instance.countRecords[i][j] * BASIC_SCORE[i][j];
                    instance.engagement += instance.netScoreRecords[i][j];
                }
            }
            instance.engagement += 10;
        }


        /**
         * Validations for tweet
         * if present - trim
         */
        if (instance.tweet) {
            const tweet = instance.tweet.toString();
            instance.tweet = tweet.trim();
            if (!instance.tweetId) {
                instance.tweetId = _.random(
                    111111111111111111,
                    999999999999999999
                ).toString();
            }
        }

        if (!instance.type) {
            instance.type = "text";
        }


        /**
         * Validation for postUserId
         */
        if (!validate(instance, currentInstance, 'postUserId', isNew)) {
            if (instance.appUserId) {
                AppUser.findOne({
                    where: { id: instance.appUserId }
                })
                    .then(function (appUser) {
                        if (appUser) {
                            if (appUser.oriUserId) {
                                if (!validate(instance, currentInstance, 'username', isNew)) {
                                    if (appUser.username) {
                                        instance.username = appUser.username;
                                    }
                                }
                                if (appUser.oriUserId !== 'admin') {
                                    if (appUser.cricUserId) {
                                        instance.postUserId = appUser.cricUserId;
                                    } else {
                                        throw new Error("Inconmplete appUserData. This user has no cricUserId attached!");
                                    }
                                }
                            }
                            return calculatePriority(instance);
                        } else {
                            throw new Error("Invalid UserId attached with this post!");
                        }
                    })
                    .then(function (priority) {
                        instance.score = priority;
                        next();
                    })
                    .catch(function (error) {
                        return next(error);
                    });
            } else {
                return next(new Error("appUserId is required!"))
            }
        } else {
            if (!validate(instance, currentInstance, 'appUserId', isNew)) {
                return next(new Error("appUserId is required!"));
            } else {
                calculatePriority(instance)
                    .then(function (priority) {
                        instance.score = priority;
                        next();
                    })
                    .catch(function (error) {
                        return next(error);
                    });
            }
        }
    }); // END - before save


    // a helper function to calculate priority score for the post
    const calculatePriority = function (instance) {
        return new Promise(function (resolve, reject) {
            if (!instance) {
                return reject(new Error("Instance data invalid!"));
            } else {
                let score = 0;
                let typeScore = 0;
                if (instance.type === 'photo') {
                    typeScore = 5;
                } else if (instance.type === 'video') {
                    typeScore = 5;
                } else if (instance.type === 'link') {
                    typeScore = 2;
                } else if (instance.type === 'text') {
                    typeScore = 4;
                } else if (instance.type === 'tweet') {
                    typeScore = 0.5;
                } else {
                    typeScore = 0;
                }
                score = instance.engagement * instance.priorityTime * typeScore;
                score = Number(score.toFixed(3));
                resolve(score);
            }
        });
    };  //calculatePriority
};
