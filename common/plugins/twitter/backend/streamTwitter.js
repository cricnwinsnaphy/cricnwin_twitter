module.exports = function (server, databaseObj, helper, packageObj) {

    const Twitter = require('twit');
    const moment = require('moment');

    const TWITTER = helper.getMainSettings("TWITTER");
    const FOLLOWERS = helper.getMainSettings("FOLLOWERS");
    const FOLLOWERS_ID = helper.getMainSettings("FOLLOWERS_ID");

    const Post = databaseObj.Post;

    const ADMIN_DETAILS = packageObj.ADMIN_DETAILS;

    const client = new Twitter({
        consumer_key: TWITTER.CONSUMER_KEY,
        consumer_secret: TWITTER.CONSUMER_SECRET,
        access_token: TWITTER.ACCESS_TOKEN_KEY,
        access_token_secret: TWITTER.ACCESS_TOKEN_SECRET,
        timeout_ms: 60 * 1000                                     // HTTP request timeout to apply to all requests.
    });


    const getTweets = function () {

        let stream = client.stream('statuses/filter', { follow: FOLLOWERS });

        // Emitted when the response is received from Twitter. The http response object is emitted.
        stream.on('connected', function (response) {
            server.logger.info("Connected to Twitter Stream", response);
        });

        // Emitted each time a status (tweet) comes into the stream.
        stream.on('tweet', function (event) {
            if (
                event.retweeted ||
                event.retweeted_status ||
                event.in_reply_to_status_id ||
                event.in_reply_to_user_id ||
                event.delete
            ) {
                // skip retweets and replies     
            }
            else {
                const postObj = {};
                const userData = {};
                let id = "";
                let id_str = "";
                const image = {
                    url: {}
                };
                const profileImage = {
                    url: {}
                };
                try {
                    id = event.user.id.toString();
                    id_str = event.user.id_str;
                    if (
                        FOLLOWERS_ID.indexOf(id_str) === -1 ||
                        FOLLOWERS_ID.indexOf(id) === -1
                    ) {
                        // Not from the listed pages
                    } else {

                        if (event.truncated) {                              // if the tweet is too long
                            postObj.tweet = event.extended_tweet.full_text;     // get the tweet message from depth
                        } else {
                            postObj.tweet = event.text;
                        }
                        if (event.extended_tweet) {
                            if (event.extended_tweet.entities) {
                                if (event.extended_tweet.entities.media && (event.extended_tweet.entities.media.length > 0)) {
                                    image.url.unSignedUrl = event.extended_tweet.entities.media[0].media_url;
                                    postObj.image = image;
                                }
                                else {
                                    if (event.entities) {
                                        if (event.entitites.media && (event.entities.media.length > 0)) {
                                            image.url.unSignedUrl = event.entities.media[0].media_url;
                                            postObj.image = image;
                                        }
                                    }
                                }
                            }
                        } else {
                            if (event.entities) {
                                if (event.entities.media && (event.entities.media.length > 0)) {
                                    image.url.unSignedUrl = event.entities.media[0].media_url;
                                    postObj.image = image;
                                }
                            }
                        }

                        postObj.tweetId = event.id_str;
                        postObj.username = "admin";
                        postObj.isAdmin = true;
                        postObj.type = "tweet";
                        postObj.twitterUser = event.user.name;
                        postObj.twitterUserId = event.user.id.toString();
                        postObj.data_raw = event;
                        profileImage.url.unSignedUrl = event.user.profile_image_url;
                        postObj.twitterUserProfilePic = profileImage;

                        userData.id = ADMIN_DETAILS.id;
                        userData.username = ADMIN_DETAILS.username;
                        userData.email = ADMIN_DETAILS.email;

                        Post.findOne({
                            where: {
                                twitterUserId: postObj.twitterUserId,
                                added: {
                                    gt: moment().subtract(2, 'minutes').toDate()
                                }
                            },
                            order: "added DESC"
                        })
                            .then(function (post) {
                                if (post) {
                                    // Invalid Tweet.. Do Nothing!
                                } else {
                                    return Post.saveTweet(postObj, userData);
                                }
                            })
                            .then(function (post) {
                                if (post) {
                                    server.logger.info('A new Tweet posted at: ' + moment().format('MMMM Do YYYY, h:mm:ss a'));
                                } else {
                                    throw new Error("Error encountered while posting new tweet with tweetId: " + postObj.tweetId);
                                }
                            })
                            .catch(function (error) {
                                server.logger.error(error);
                            });
                    }
                } catch (error) {
                    server.logger.error("An error occured!", error);
                    // Something went wrong
                }
            }
        });

        // Emitted when an API request or response error occurs.
        stream.on('error', function (error) {
            server.logger.error("Error encountered at: " + moment().format('MMMM Do YYYY, h:mm:ss a') + "\n");
            server.logger.error("Error: ", error);
            stream.stop();
            // setTimeout(getTweets, timeoutInterval);
        });

        // Emitted when a disconnect message comes from Twitter. This occurs if you have multiple streams 
        // connected to Twitter's API. Upon receiving a disconnect message from Twitter, Twit will close the 
        // connection and emit this event with the message details received from twitter.
        stream.on('disconnect', function (disconnectedMessage) {
            server.logger.error("Disconnected at: " + moment().format('MMMM Do YYYY, h:mm:ss a') + "\n");
            server.logger.error("Reason: ", disconnectedMessage);
            stream.stop();
            // setTimeout(getTweets, timeoutInterval);
        });


    };

    return {
        getTweets: getTweets
    };

};  // module.exports