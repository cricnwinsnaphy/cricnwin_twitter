module.exports = function (server, databaseObj, helper, packageObj) {
    'use strict';

    const Promise = require('bluebird');
    const moment = require('moment');
    const request = require('request-promise');

    const appUserRemoteMethods = require('./appUserRemoteMethods')(server, databaseObj, helper, packageObj);

    const Post = databaseObj.Post;               // get Post Model
  


    /**
     * Remote Method - setPost
     * Will create a record of Post with data sent from frontend
     * If the user who posted the record is not registered in the backend,
     * add a record of that user in the database
     */
    const saveTweet = function (postObj, userData) {
        const response = {};
        return new Promise(function (resolve, reject) {
            if (!postObj || !userData) {
                reject("Data could not be fetched from frontend");
            }
            appUserRemoteMethods.findOrAddUser(userData)
            .then(function(appuserData){
                if(appuserData){
                    postObj.appUserId = appuserData.id;
                    postObj.profilePic = appuserData.profilePic;
                    if(!postObj.postUserId){
                        postObj.postUserId = appuserData.cricUserId;
                    }           
                    return Post.create(postObj);
                }else {
                    throw new Error("Error creating new User before saving Post.");
                }
            })
            .then(function(savedPostObj){
                if(savedPostObj){
                    resolve(savedPostObj);
                }else{
                    throw new Error("Error saving Post...");
                }
            })
            .catch(function(error){
                reject(error);
            });
        });
    };  // setPost

    /**
     * Registering remoteMethod - fetchPosts
     */
    const saveTweetMethod = function () {
        Post.saveTweet = saveTweet;
        Post.remoteMethod('setPost', {
            accepts: [
                { arg: 'postObj', type: 'object' },
                { arg: 'userData', type: 'object' }
            ],
            returns: { arg: 'response', type: 'object', root: true },
            http: { path: '/setPost', verb: 'post' }
        });
    };


    return {
        saveTweetMethod: saveTweetMethod
    };

};