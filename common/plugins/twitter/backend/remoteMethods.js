module.exports = function(server, databaseObj, helper, packageObj){
    'use strict';

    const postRemoteMethods = require('./postRemoteMethods')(server, databaseObj, helper, packageObj);
    const streamTwitter = require('./streamTwitter')(server, databaseObj, helper, packageObj);

    const init = function(){
        postRemoteMethods.saveTweetMethod();
        streamTwitter.getTweets();
    };

    return {
        init : init
    };
};  // module.exports