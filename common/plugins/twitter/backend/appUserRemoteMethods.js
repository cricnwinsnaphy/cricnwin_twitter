module.exports = function(server, databaseObj, helper, packageObj){
    'use strict';

    const Promise = require('bluebird');
    const request = require('request-promise');

    const AppUser = databaseObj.AppUser;

    /**
     * Helper Method - findOrAddUser
     * Will find a user matching the details passed in userData argument
     * If user is not found in database, create a new user with passed details
     * @param {*} userData 
     * @returns {*} userObj - AppUser instance
     */
    const findOrAddUser = function (userData) {
        return new Promise(function (resolve, reject) {
            if (!userData) {
                return reject(new Error("Invalid UserData"));
            }
            if(userData.username === 'admin'){
                AppUser.findOne({
                    where: { oriUserId: userData.id }
                })
                    .then(function (appuserData) {
                        if (!appuserData) {
                            return AppUser.create({
                                email: userData.email,
                                oriUserId: userData.id,
                                username: userData.username,
                                profilePic: userData.profilePic
                            });
                        } else {
                            return appuserData;
                        }
                    }).then(function (userObj) {
                        resolve(userObj);
                    }).catch(function (err) {
                        reject(err);
                    });
            }else {
                AppUser.findOne({
                    where: { cricUserId: userData.cricUserId }
                })
                    .then(function (appuserData) {
                        if (!appuserData) {
                            return AppUser.create({
                                email: userData.email,
                                oriUserId: userData.id,
                                cricUserId: userData.cricUserId,
                                username: userData.username,
                                profilePic: userData.profilePic
                            });
                        } else {
                            return appuserData;
                        }
                    }).then(function (userObj) {
                        resolve(userObj);
                    }).catch(function (err) {
                        reject(err);
                    });
            }
        });
    };

    return {
        findOrAddUser: findOrAddUser
    };
};  // module.exports