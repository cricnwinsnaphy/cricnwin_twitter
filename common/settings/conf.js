const {join} =  require("path");

module.exports = (server) => {
    const NAME = "SnaphyDemo";
    const DESCRIPTION = "Snaphy Control Panel";
    const ANGULAR_MODULE = "snaphy";
    const VERSION =  "1.0.0";
    const AUTHOR =  "Robins Gupta";
    const PLUGIN_PATH =  join(__dirname, "../../common/plugins");
    const SERVER_PATH =  join(__dirname, "../../server/server.js");
    const SERVER_FOLDER =  join(__dirname, "../../server");
    const MODEL_PATH = join(__dirname, "../../common/models");
    const VALIDATION_PATH = join(__dirname, "../../common/validations");
    const TABLE_PATH = join(__dirname, "../../common/table");
    const SETTING_PATH = join(__dirname, "../../common/settings");


    //Write all the plugins name whose load priority is to be set...
    const PLUGIN_PRIORITY = ["cache", "dashboard", "login"];


    // Followers ID Order: 
    // 1. @SunRisers, 2. @DelhiDaredevils, 3. @ChennaiIPL, 4. @mipaltan, 5. @RCBTweets, 
    // 6. @lionsdenkxip, 7. @KKRiders, 8. @rajasthanroyals, 9. @IPL, 10. @hemantvayali,
    // 11. @iamjadeja, 12. @iamraina, 13. @gautamgambhir, 14. @bhogleharsha, 15.@scganguly,
    // 16. @msdhoni, 17. @imVkohli, 18. @ashwinravi99, 19. @sachin_rt, 20. @virendersehwag,
    // 21. @cricketaakash, 22. @sanjaymanjrekar, 23. @BCCI, 24. @CricketAus, 25. @OfficialCSA,
    // 26. @englandcricket, 27. @ABdeVilliers17, 28. @VVSLaxman281, 29. @ICC
    const FOLLOWERS = '989137039, 176888549, 117407834, 106345557, 70931004, 30631766, 23592970, 17082958, 15639696, 482756677, 1293920390, 58841047, 99448420, 78941611, 97169575, 92708272, 71201743, 358814014, 135421739, 92724677, 70663487, 176295495, 185142711, 17692554, 45847467, 3017241909, 139876086, 2500955780, 177547780';
    const FOLLOWERS_ID = ["117407834", "176888549", "117407834", "106345557", "70931004", "30631766", "23592970", "17082958", "15639696", "482756677", "58841047", "1293920390", "99448420", "78941611", "97169575", "92708272", "71201743", "358814014", "135421739", "92724677", "17692554", "70663487", "176295495", "185142711", "45847467", "3017241909", "139876086", "2500955780", "177547780"];

    // Write all the twitter api related keys here...
    const TWITTER = {
        CONSUMER_KEY : "r8IWEfkX0wUR326ruK8oVdrDd",
        CONSUMER_SECRET: "Yze76ZBjYT7ZCIYydGquDcKPZBMgRQQyHgwvgxjAlyA7dvKbJN",
        ACCESS_TOKEN_KEY: "4048151593-21MH1VqXlEv7Iyh8VYJRvdcspR6nUBDsaBFxdn4",
        ACCESS_TOKEN_SECRET: "MaIrjpYfmDMwapc3IaW5JIL9o9M3xGuVjHrnuAkhpTJHC"
    };

    const ROWS = 3;
    const COLS = 4;

    const INDICES = {
        "like": 0,
        "comment": 1,
        "share": 2,
        "follow": 0,
        "follower": 1,
        "friends": 2,
        "other": 3
    };

    const TYPE_OBJ = {
        "follow": "follow",
        "follower": "follower",
        "friends": "friends",
        "other": "other"
    };

    const BASIC_SCORE = [
        [0.2, 0.4, 0.5, 0.1],
        [0.6, 2, 2.5, 0.5],
        [1.8, 8, 10, 2]
    ];

    return {
        NAME,
        PLUGIN_PATH,
        DESCRIPTION,
        ANGULAR_MODULE,
        SERVER_PATH,
        SERVER_FOLDER,
        MODEL_PATH,
        VALIDATION_PATH,
        TABLE_PATH,
        SETTING_PATH,
        PLUGIN_PRIORITY,
        VERSION,
        AUTHOR,
        FOLLOWERS,
        FOLLOWERS_ID,
        TWITTER,
        INDICES, 
        ROWS,
        COLS,
        TYPE_OBJ,
        BASIC_SCORE
    }
};
